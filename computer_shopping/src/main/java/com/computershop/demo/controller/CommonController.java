package com.computershop.demo.controller;

import cn.hutool.Hutool;
import cn.hutool.core.util.ObjectUtil;
import com.computershop.demo.service.*;
import com.computershop.demo.vo.BRequest;
import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.*;
import com.computershop.demo.vo.req.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/2 11:19
 * @file: Common
 * @description: 星期三
 */
@RestController
@RequestMapping("sys/")
public class CommonController {


    @Autowired
    ComputerService computerService;

    @Autowired
    OrderService orderService;

    @Autowired
    ShippingAddressService shippingAddressService;

    @Autowired
    PhoneService phoneService;

    @Autowired
    TabletComputerService tabletComputerService;

    @Autowired
    AccessoriesService accessoriesService;

    @ApiOperation(tags = "查询电脑信息接口", value = "query_computer_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "computerName", value = "电脑名称", dataType = "String", paramType = "query", example = "联想拯救者"),
            @ApiImplicitParam(name = "price", value = "电脑价格", dataType = "float", paramType = "query", example = "5000.0"),
            @ApiImplicitParam(name = "type", value = "类型 0全部 1联想 2戴尔 3惠普 4 其他", dataType = "Integer", paramType = "query", example = "1"),
            @ApiImplicitParam(name = "sorted", value = "类型 0全部 1联想 2戴尔 3惠普 4 其他", dataType = "Integer", paramType = "query", example = "1"),
    })
    @RequestMapping(value = "query_computer_info",method = RequestMethod.POST)
    BResponse<List<Computer>> queryComputerByPage(@RequestBody() BRequest<ReqComputer> request) {
        check(request);
        return computerService.queryComputerByPage(request.getData());
    }

    @ApiOperation(tags = "查询电脑详情接口", value = "query_computer_detail_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "computerName", value = "computerId", dataType = "Integer", paramType = "query", example = "15", required = true),
    })
    @RequestMapping(value = "query_computer_detail_info",method = RequestMethod.POST)
    BResponse<ComputerDetail> queryComputerDetail(@RequestBody() BRequest<Integer> request) {
        check(request);
        return computerService.queryComputerDetail(request.getData());
    }


    @ApiOperation(tags = "查询手机信息接口", value = "query_phone_info")
    @RequestMapping(value = "query_phone_info",method = RequestMethod.POST)
    BResponse<List<Phone>> queryPhoneByPage(@RequestBody()BRequest<ReqPhone> request) {
        check(request);
        return phoneService.queryPhoneByPage(request.getData());
    }

    @ApiOperation(tags = "查询手机详情接口", value = "query_phone_detail_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "手机ID", value = "phoneId", dataType = "Integer", paramType = "query", example = "15", required = true),
    })
    @RequestMapping(value = "query_phone_detail_info",method = RequestMethod.POST)
    BResponse<PhoneDetail> queryPhoneDetail(@RequestBody() BRequest<Integer> request) {
        check(request);
        return phoneService.queryPhoneDetail(request.getData());
    }

    @ApiOperation(tags = "查询平板电脑信息接口", value = "query_tablet_computer_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "平板名称", value = "name", dataType = "String", paramType = "query", example = "键盘", required = true),
            @ApiImplicitParam(name = "价格升序/降序", value = "sorted", dataType = "int", paramType = "query", example = "2", required = true),
    })
    @RequestMapping(value = "query_tablet_computer_info",method = RequestMethod.POST)
    BResponse<List<TabletComputer>> queryTabletComputerByPage(@RequestBody() BRequest<ReqTabletComputer> request) {
        check(request);
        return tabletComputerService.queryTabletComputerByPage(request.getData());
    }

    @ApiOperation(tags = "查询零件信息接口", value = "query_accessories_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "零件名称", value = "name", dataType = "String", paramType = "query", example = "键盘", required = true),
            @ApiImplicitParam(name = "价格升序/降序", value = "sorted", dataType = "int", paramType = "query", example = "2", required = true),
    })
    @RequestMapping(value = "query_accessories_info",method = RequestMethod.POST)
    BResponse<List<Accessories>> queryAccessoriesByPage(@RequestBody()BRequest<ReqAccessories> request) {
        check(request);
        return accessoriesService.queryAccessoriesByPage(request.getData());
    }

    @ApiOperation(tags = "查询零件详情信息接口", value = "query_accessories_info")
    @RequestMapping(value = "query_accessories_detail_info",method = RequestMethod.POST)
    BResponse<AccessoriesDetail> queryAccessoriesDetail(@RequestBody()BRequest<Integer> request) {
        check(request);
        return accessoriesService.queryAccessorieDetail(request.getData());
    }

    @ApiOperation(tags = "查询平板详情信息接口", value = "query_accessories_info")
    @RequestMapping(value = "query_tablet_computer_detail_info",method = RequestMethod.POST)
    BResponse<TabletComputerDetail> queryTabletComputerDetail(@RequestBody()BRequest<Integer> request) {
        check(request);
        return tabletComputerService.queryTabletComputerDetail(request.getData());
    }
//
    /**
     * 分页查询订单信息
     **/
    @ApiOperation(tags = "分页查询订单信息", value = "query_order_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", dataType = "String", paramType = "query", example = "ADADADAGA12", required = true),
            @ApiImplicitParam(name = "payable", value = "支付状态", dataType = "Integer", paramType = "query", example = "1")
    })
    @RequestMapping(value = "query_order_info",method = RequestMethod.POST)
    BResponse<List<Order>> queryOrderPage(@RequestBody() BRequest<ReqOrder> request) {
        check(request);
        return orderService.queryOrderPage(request.getData());
    }

    /**
     * 通过id删除订单
     **/
    @ApiOperation(tags = "通过id删除订单", value = "delete_order_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", dataType = "Integer", paramType = "query", example = "15", required = true),
    })
    @RequestMapping(value = "delete_order_info",method = RequestMethod.POST)
    BResponse<Integer> deleteOrderById(@RequestBody() BRequest<Integer> request) {
        check(request);
        return orderService.deleteOrderById(request.getData());
    }

    /**
     * 创建订单
     **/
    @ApiOperation(tags = "创建订单", value = "create_order")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderName", value = "订单名称", dataType = "String", paramType = "query", example = "联想拯救者Y7000", required = true),
            @ApiImplicitParam(name = "orderTime", value = "下单时间", dataType = "timestamp", paramType = "query", example = "2020-08-16 15:32:12", required = true),
            @ApiImplicitParam(name = "accountPayable", value = "应付金额", dataType = "float", paramType = "query", example = "1", required = true),
            @ApiImplicitParam(name = "address", value = "收货地址", dataType = "String", paramType = "query", example = "1", required = true),
            @ApiImplicitParam(name = "combo", value = "套餐", dataType = "String", paramType = "query", example = "官方标配", required = true)

    })
    @RequestMapping(value = "create_order",method = RequestMethod.POST)
    BResponse<Integer> createOrderInfo(@RequestBody() BRequest<Order> request) {
        check(request);
        return orderService.createOrderInfo(request.getData());
    }

    @ApiOperation(tags = "支付接口", value = "pay")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", dataType = "Integer", paramType = "query", example = "15", required = true),
    })
    @RequestMapping(value = "pay",method = RequestMethod.POST)
    BResponse<Integer> payActual(@RequestBody() BRequest<Integer> request) {
        check(request);
        return orderService.payActual(request.getData());
    }

    @ApiOperation(tags = "查询收货地址信息接口", value = "query_shipping_address")
    @RequestMapping(value = "query_shipping_address",method = RequestMethod.POST)
    BResponse<List<ShippingAddress>> queryShippingAddress() {
        return shippingAddressService.queryShippingAddress();
    }


    @ApiOperation(tags = "新增收货地址接口", value = "insert_shipping_address")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", dataType = "String", paramType = "query", example = "ADADADADA2", required = true),
            @ApiImplicitParam(name = "tel", value = "用户电话", dataType = "String", paramType = "query", example = "15983515503", required = true),
            @ApiImplicitParam(name = "address", value = "收货地址", dataType = "String", paramType = "query", example = "成都市龙泉驿区成都大学", required = true),
    })
    @RequestMapping(value = "insert_shipping_address",method = RequestMethod.POST)
    BResponse<Integer> insertAddress(@RequestBody() BRequest<ShippingAddress> request) {
        check(request);
        BResponse<Integer> response = shippingAddressService.insertAddress(request.getData());
        return response;
    }

    @ApiOperation(tags = "删除收货地址", value = "delete_shipping_address")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "收货地址id", dataType = "Integer", paramType = "query", example = "15", required = true),
    })
    @RequestMapping(value = "delete_shipping_address",method = RequestMethod.POST)
    BResponse<Integer> deleteAddress(@RequestBody() BRequest<Integer> request) {
         check(request);
         return shippingAddressService.deleteAddress(request.getData());
    }

    public void check(Object o) {
        if (ObjectUtil.isNull(o)) {
            throw new NullPointerException("参数错误");
        }
    }
}
