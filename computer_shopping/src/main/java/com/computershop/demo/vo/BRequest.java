package com.computershop.demo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 11:59
 * @file: TRequest
 * @description: 星期二
 */
@Data
public class BRequest<T>{

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp timestamp;

    private T data;

}
