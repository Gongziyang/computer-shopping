package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 12:48
 * @file: Computer
 * @description: 星期二
 */
@Data
public class Computer extends Page{

    private int id;

    private String name;

    private float price;

    private int type;

    private String url;

}
