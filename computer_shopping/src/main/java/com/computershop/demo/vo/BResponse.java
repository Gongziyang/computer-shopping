package com.computershop.demo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 11:59
 * @file: BResponse
 * @description: 星期二
 */
@Data
public class BResponse<T>{

    private  static int ERROR = -1;

    private static int SUCCESS = 0;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String timestamp;

    /**
     * 响应说明
     * **/
    private String msg;

    /**
     * 状态码
     * **/
    private int code;

    private T data;

    private int itemSize;


    public static <T> BResponse<T> error(String msg,T t){
        BResponse<T> response = new BResponse<>();
        response.setTimestamp(createTimestamp());
        response.setCode(500);
        response.setMsg(msg);
        response.setData(t);
        return response;
    }

    public static <T> BResponse<T> success(T t){
        BResponse<T> response = new BResponse<>();
        response.setTimestamp(createTimestamp());
        response.setCode(SUCCESS);
        response.setMsg("操作成功");
        response.setData(t);
        return response;
    }

    public static <T> BResponse<T> success(int result,T t){
        BResponse<T> response = new BResponse<>();
        response.setTimestamp(createTimestamp());
        response.setCode(200);
        response.setMsg("操作成功");
        response.setData(t);
        response.setItemSize(result);
        return response;
    }

    public static String createTimestamp(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = dateFormat.format(new Date(System.currentTimeMillis()));
        return format;
    }

}
