package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:49
 * @file: PhoneDetail
 * @description: 星期四
 */
@Data
public class PhoneDetail {

    private int id;

    private String name;

    private int accessMemory;

    private String cpuType;

    private String description;

    private int refId;

    private int ssd;

    private float price;

    private String url;

}
