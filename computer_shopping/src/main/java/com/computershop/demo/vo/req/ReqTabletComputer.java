package com.computershop.demo.vo.req;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 18:01
 * @file: ReqTabletComputer
 * @description: 星期四
 */
@Data
public class ReqTabletComputer {

    private String name;

    private int sorted;
}
