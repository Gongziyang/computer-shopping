package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 17:57
 * @file: TabletComputerDetail
 * @description: 星期四
 */
@Data
public class TabletComputerDetail {

    private int id;

    private String name;

    private String info;

    private String url;

    private float price;
}
