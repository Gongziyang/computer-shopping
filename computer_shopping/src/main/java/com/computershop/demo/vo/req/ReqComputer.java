package com.computershop.demo.vo.req;

import com.computershop.demo.vo.common.Page;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 18:19
 * @file: ReqComputer
 * @description: 星期二
 */
@Data
public class ReqComputer extends Page {

    private String computerName;

    /**
     * 期望价格
     */
    private float price;

    /**
     * 电脑类型  0 全部   1联想  2惠普  3戴尔  4其他
     * **/
    private int type;

    int sorted;

}
