package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 17:27
 * @file: AccessoriesDetail
 * @description: 星期四
 */
@Data
public class AccessoriesDetail {

    private int id;

    private String name;

    private String url;

    private String info;

    private float price;

}
