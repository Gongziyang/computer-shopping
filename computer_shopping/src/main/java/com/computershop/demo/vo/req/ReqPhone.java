package com.computershop.demo.vo.req;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 16:25
 * @file: ReqPhone
 * @description: 星期四
 */
@Data
public class ReqPhone {

    private String phoneName;

    /**
     * 期望价格
     */
    private float price;

    /**
     * 电脑类型
     * 0 显示全部
     * 1 == 小米
     * 2 == 华为
     * 3 == vivo
     * 4 == oppo
     * 5 == 苹果
     * **/
    private int type;

    int sorted;
}
