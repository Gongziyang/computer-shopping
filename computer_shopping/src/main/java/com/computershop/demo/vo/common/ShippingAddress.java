package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 12:59
 * @file: ShippingAddress
 * @description: 星期二
 */
@Data
public class ShippingAddress extends Page{

    private int id;

    private String userId;

    private String tel;

    private String address;

}
