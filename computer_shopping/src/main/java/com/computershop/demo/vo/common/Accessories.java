package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:37
 * @file: Accessories
 * @description: 附件(鼠标等)
 */
@Data
public class Accessories {

    private int id;

    private String name;

    private float price;

    private String url;

}
