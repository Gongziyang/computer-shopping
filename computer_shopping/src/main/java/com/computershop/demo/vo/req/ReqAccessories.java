package com.computershop.demo.vo.req;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 17:26
 * @file: ReqAccessories
 * @description: 星期四
 */
@Data
public class ReqAccessories {

    private String name;

    private int type;

    int sorted;
}
