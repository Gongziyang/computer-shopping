package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/2 0:05
 * @file: Page
 * @description: 星期三
 */
@Data
public class Page {

    private int pageNo;

    private int pageSize;

}
