package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:38
 * @file: TabletComputer
 * @description: 星期四
 */
@Data
public class TabletComputer {

    private int id;

    private String name;

    private float price;

    private String url;

    private String info;
}
