package com.computershop.demo.vo.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:25
 * @file: Order
 * @description: 订单信息实体
 */
@Data
@ApiModel(value = "订单信息")
public class Order extends Page implements Comparable<Order>{

    private int id;

    /**
     * 订单编号
     **/
    @ApiModelProperty(value = "订单编号")
    private String orderNumber;
    /**
     * 订单名称
     **/
    @ApiModelProperty(value = "订单名称")
    private String orderName;
    /**
     * 订单信息
     **/
    @ApiModelProperty(value = "订单信息")
    private String orderInfo;
    /**
     * 下单时间，用JsonFormat来转换为timestamp类型
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "下单时间")
    private String orderTime;
    /**
     * 应付金额
     **/
    @ApiModelProperty(value = "应付金额")
    private float accountPayable;

    /**
     * 订单状态
     **/
    @ApiModelProperty(value = "已支付0 待支付1")
    private int state;

//    /**
//     * 关联用户
//     **/
//    @ApiModelProperty(value = "关联用户")
//    private String userId;

    /**
     * 收货地址
     **/
    @ApiModelProperty(value = "收货地址")
    private String address;

    /**
     * 套餐
     **/
    @ApiModelProperty(value = "套餐")
    private String combo;


    @Override
    public String toString() {
        return  "{"
                +"\""+"orderName"+"\""+":"+"\""+this.orderName+"\""+","
                +"\""+"orderNumber"+"\""+":"+"\""+this.orderNumber+"\""+","
                +"\""+"accountPayable"+"\""+":"+this.accountPayable+","
                +"\""+"orderTime"+"\""+":"+"\""+this.orderTime+"\""+","
                +"\""+"state"+"\""+":"+"\""+(this.state == 0?"未支付":"已支付")+"\""+","
                +"\""+"orderId"+"\""+":"+this.id+","
                +"\""+"address"+"\""+":"+"\""+this.address+"\""+","
                +"\""+"combo"+"\""+":"+"\""+this.combo+"\""+
                "}";
    }

    @Override
    public int compareTo(Order o) {
        return this.getId() - o.getId();
    }
}
