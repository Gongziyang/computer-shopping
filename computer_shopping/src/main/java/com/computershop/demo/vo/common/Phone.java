package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:40
 * @file: Phone
 * @description: 星期四
 */
@Data
public class Phone {

    private int id;

    private String name;

    private int price;

    private String url;

    private int type;
}
