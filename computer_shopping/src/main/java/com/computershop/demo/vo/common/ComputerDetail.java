package com.computershop.demo.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 12:56
 * @file: ComputerDetail
 * @description: 星期二
 */
@Data
public class ComputerDetail {

    private int id;

    private String name;

    private int accessMemory;

    private String displayCard;

    private String description;

    private int refId;

    private String url;

    private float price;
}
