package com.computershop.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 18:19
 * @file: ReqOrder
 * @description: 星期二
 */
@Data
public class ReqOrder {

    /**
     * 用户ID
     **/
    @ApiModelProperty(value = "用户ID")
    private String userId = null;
    /**
     * 订单状态
     **/
    @ApiModelProperty(value = "订单状态")
    private int payable = -1;
}
