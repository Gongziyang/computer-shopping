package com.computershop.demo.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 13:05
 * @file: MyDataSource
 * @description: 星期二
 */
@Configuration
public class MyDataSource {

    private String url = "jdbc:mysql://localhost:3306/computer_shopping?serverTimezone=GMT%2B8";
    private String driver = "com.mysql.cj.jdbc.Driver";
    private String user = "root";
    private String passWord = "yyf1998";

    @Bean
    public SqlSessionFactory sqlSessionFactoryBean() throws Exception {
        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(druidDataSource());
        Resource[] resources = new PathMatchingResourcePatternResolver()
                .getResources("classpath:mapper/**/*.xml");
        Resource resource = new PathMatchingResourcePatternResolver()
                .getResource("classpath:mybatis-config.xml");
        sessionFactoryBean.setConfigLocation(resource);
        sessionFactoryBean.setMapperLocations(resources);
        return sessionFactoryBean.getObject();
    }

    @Bean
    public DruidDataSource druidDataSource(){
         DruidDataSource dataSource = new DruidDataSource();
         dataSource.setUrl(url);
         dataSource.setDriverClassName(driver);
         dataSource.setUsername(user);
         dataSource.setPassword(passWord);
         return dataSource;
    }
}
