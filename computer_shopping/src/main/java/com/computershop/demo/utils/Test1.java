package com.computershop.demo.utils;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/16 20:43
 * @file: Test1
 * @description: 星期三
 */
@FunctionalInterface
public interface Test1 {
    public static void main(String[] args) {
        Test1 t1 = new Test()::mt;
        t1.get();

    }

    void get();
}
