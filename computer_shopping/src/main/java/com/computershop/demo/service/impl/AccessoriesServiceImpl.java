package com.computershop.demo.service.impl;

import com.computershop.demo.mapper.AccessoriesMapper;
import com.computershop.demo.service.AccessoriesService;
import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Accessories;
import com.computershop.demo.vo.common.AccessoriesDetail;
import com.computershop.demo.vo.req.ReqAccessories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:53
 * @file: AccessoriesServiceImpl
 * @description: 星期四
 */
@Service
public class AccessoriesServiceImpl implements AccessoriesService {

    @Autowired
    AccessoriesMapper accessoriesMapper;

    @Override
    public BResponse<List<Accessories>> queryAccessoriesByPage(ReqAccessories reqAccessories) {
        return BResponse.success(accessoriesMapper.queryAccessoriesByPage(reqAccessories
        ));
    }

    @Override
    public BResponse<AccessoriesDetail> queryAccessorieDetail(Integer id) {
        return null;
    }
}
