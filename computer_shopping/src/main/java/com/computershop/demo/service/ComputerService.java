package com.computershop.demo.service;

import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Computer;
import com.computershop.demo.vo.common.ComputerDetail;
import com.computershop.demo.vo.req.ReqComputer;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 13:01
 * @file: ComputerService
 * @description: 星期二
 */
public interface ComputerService {

    BResponse<List<Computer>> queryComputerByPage(ReqComputer reqComputer);

    BResponse<ComputerDetail> queryComputerDetail(Integer computerId);


}
