package com.computershop.demo.service.impl;

import com.computershop.demo.mapper.OrderMapper;
import com.computershop.demo.service.OrderService;
import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Order;
import com.computershop.demo.vo.req.ReqOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/2 9:36
 * @file: OrderServiceImpl
 * @description: 星期三
 */
@Service
public class OrderServiceImpl implements OrderService {


    @Autowired
    OrderMapper orderMapper;


    @Override
    public BResponse<List<Order>> queryOrderPage(ReqOrder request) {
        List<Order> orders = orderMapper.queryOrderPage(request);
        int itemSize = orderMapper.itemSize(request);
        if(orders == null){
            return BResponse.error("暂无更多信息",null);
        }
        return BResponse.success(itemSize,orders);
    }

    @Override
    public BResponse<Integer> deleteOrderById(int orderId) {
        int result = orderMapper.deleteOrderById(orderId);
        if(result == 0){
            return BResponse.error("删除失败",-1);
        }
        return BResponse.success(result);
    }


    @Override
    public BResponse<Integer> createOrderInfo(Order order) {
        order.setOrderNumber(getHexString());
        order.setOrderInfo(order.toString());
        int result = orderMapper.createOrderInfo(order);
        if(result == 0){
            return BResponse.error("创建订单失败",-1);
        }
        return BResponse.success(order.getId());
    }

    @Override
    public BResponse<Integer> payActual(Integer orderId) {
        Order order = orderMapper.queryOrderById(orderId);
        if(order == null){
            return BResponse.error("支付异常，找不到订单",null);
        }
        order.setState(1);
        Map<String,Object> map = new HashMap<>(2);
        String orderInfo =order.toString();
        map.put("id",orderId);
        map.put("orderInfo",orderInfo);
        int result1 = orderMapper.payActual(map);
        if(result1 ==0){
            return BResponse.error("支付异常",null);
        }
        return BResponse.success(result1);
    }

    public String getHexString(){
        int random = new Random().nextInt(10000) + 150;
        StringBuffer sbf = new StringBuffer();
        while(random != 0){
             int i = random%10 + 1100;
             random = random / 10;
             sbf.append(Integer.toHexString(i));
        }
        return sbf.toString();
    }

}
