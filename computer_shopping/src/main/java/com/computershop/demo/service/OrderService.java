package com.computershop.demo.service;

import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Order;
import com.computershop.demo.vo.req.ReqOrder;
import io.swagger.models.auth.In;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 13:02
 * @file: OrderService
 * @description: 星期二
 */
public interface OrderService {

    /**
     * 分页查询订单信息
     * **/
    BResponse<List<Order>> queryOrderPage(ReqOrder request);

    /**
     * 通过id删除订单
     * @param orderId 订单id
     * **/
    BResponse<Integer> deleteOrderById(int orderId);

    /** 创建订单 **/
    BResponse<Integer> createOrderInfo(Order order);

    BResponse<Integer> payActual(Integer orderId);
}
