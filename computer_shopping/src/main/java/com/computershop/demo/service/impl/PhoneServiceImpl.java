package com.computershop.demo.service.impl;

import com.computershop.demo.mapper.PhoneMapper;
import com.computershop.demo.service.PhoneService;
import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Phone;
import com.computershop.demo.vo.common.PhoneDetail;
import com.computershop.demo.vo.req.ReqPhone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:55
 * @file: PhoneServiceImpl
 * @description: 星期四
 */
@Service
public class PhoneServiceImpl implements PhoneService {

    @Autowired
    PhoneMapper phoneMapper;
    @Override
    public BResponse<List<Phone>> queryPhoneByPage(ReqPhone reqPhone) {
        return BResponse.success(phoneMapper.queryPhoneByPage(reqPhone));
    }

    @Override
    public BResponse<PhoneDetail> queryPhoneDetail(Integer phoneId) {
        return BResponse.success(phoneMapper.queryPhoneDetail(phoneId));
    }

}
