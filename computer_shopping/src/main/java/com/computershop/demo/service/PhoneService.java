package com.computershop.demo.service;

import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Phone;
import com.computershop.demo.vo.common.PhoneDetail;
import com.computershop.demo.vo.req.ReqPhone;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:53
 * @file: PhoneService
 * @description: 星期四
 */
public interface PhoneService {

    BResponse<List<Phone>> queryPhoneByPage(ReqPhone reqPhone);

    BResponse<PhoneDetail> queryPhoneDetail(Integer phoneId);

}
