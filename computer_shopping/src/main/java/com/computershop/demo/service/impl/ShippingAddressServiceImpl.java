package com.computershop.demo.service.impl;

import com.computershop.demo.mapper.ShippingMapper;
import com.computershop.demo.service.ShippingAddressService;
import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.ShippingAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/2 9:36
 * @file: ShippingAddressServiceImpl
 * @description: 星期三
 */
@Service
public class ShippingAddressServiceImpl implements ShippingAddressService {

    @Autowired
    ShippingMapper shippingMapper;

    @Override
    public BResponse<List<ShippingAddress>> queryShippingAddress() {
        List<ShippingAddress> shippingAddresses = shippingMapper.queryShippingAddress();
        if (shippingAddresses == null) {
            return BResponse.error("暂无更多信息，请添加收货地址", null);
        }
        return BResponse.success(shippingAddresses.size(), shippingAddresses);
    }

    @Override
    public BResponse<Integer> itemSize() {
        int itemSize = shippingMapper.itemSize();
        return BResponse.success(itemSize);
    }

    @Override
    public BResponse<Integer> insertAddress(ShippingAddress shippingAddress) {
        if (shippingAddress.getAddress() == null || shippingAddress.getAddress() == ""
                || shippingAddress.getTel() == null || shippingAddress.getTel() == ""
                || shippingAddress.getUserId() == null || shippingAddress.getUserId() == "") {
            return BResponse.error("请输入完整的地址信息",null);
        }
        int result = shippingMapper.insertAddress(shippingAddress);
        return BResponse.success(result);
    }

    @Override
    public BResponse<Integer> deleteAddress(Integer id) {
        int result = shippingMapper.deleteAddress(id);
        if(result == 0){
            return BResponse.error("删除失败",-1);
        }
        return BResponse.success(result);
    }

}
