package com.computershop.demo.service;

import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.TabletComputer;
import com.computershop.demo.vo.common.TabletComputerDetail;
import com.computershop.demo.vo.req.ReqTabletComputer;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:52
 * @file: TabletComputerService
 * @description: 星期四
 */
public interface TabletComputerService {

    BResponse<List<TabletComputer>> queryTabletComputerByPage(ReqTabletComputer reqTabletComputer);

    BResponse<TabletComputerDetail> queryTabletComputerDetail(Integer id);
}
