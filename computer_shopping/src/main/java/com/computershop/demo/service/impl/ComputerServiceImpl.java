package com.computershop.demo.service.impl;

import com.computershop.demo.mapper.ComputerMapper;
import com.computershop.demo.service.ComputerService;
import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Computer;
import com.computershop.demo.vo.common.ComputerDetail;
import com.computershop.demo.vo.req.ReqComputer;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/2 0:02
 * @file: ComputerServiceImpl
 * @description: 星期三
 */
@Service
public class ComputerServiceImpl implements ComputerService {


    @Autowired
    ComputerMapper computerMapper;

    @Override
    public BResponse<List<Computer>> queryComputerByPage(ReqComputer reqComputer) {
        int pageNo = reqComputer.getPageNo();
        int pageSize = reqComputer.getPageSize();
        if(pageNo == 0 && pageSize == 0){
            pageNo = 1;
            pageSize = 5;
        }
//        PageHelper.startPage(pageNo,pageSize);
        List<Computer> computers = computerMapper.queryComputerByPage(reqComputer);
        int itemSize = computerMapper.itemSize(reqComputer);
        if(computers == null){
            return BResponse.error("暂无更多信息，请耐心等待商品上架",null);
        }
        return  BResponse.success(itemSize,computers);
    }

    @Override
    public BResponse<ComputerDetail> queryComputerDetail(Integer computerId) {
        ComputerDetail computerDetail = computerMapper.queryComputerDetail(computerId);
        if(computerDetail == null){
            return BResponse.error("查询失败",null);
        }
        return BResponse.success(computerDetail);
    }

}
