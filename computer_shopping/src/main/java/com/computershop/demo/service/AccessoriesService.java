package com.computershop.demo.service;

import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.Accessories;
import com.computershop.demo.vo.common.AccessoriesDetail;
import com.computershop.demo.vo.req.ReqAccessories;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:52
 * @file: AccessoriesService
 * @description: 星期四
 */
public interface AccessoriesService {

    BResponse<List<Accessories>> queryAccessoriesByPage(ReqAccessories reqAccessories);

    BResponse<AccessoriesDetail> queryAccessorieDetail(Integer id);
}
