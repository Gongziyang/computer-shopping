package com.computershop.demo.service;

import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.ShippingAddress;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 13:04
 * @file: ShippingAddressService
 * @description: 星期二
 */
public interface ShippingAddressService {

    BResponse<List<ShippingAddress>> queryShippingAddress();

    BResponse<Integer> itemSize();

    BResponse<Integer> insertAddress(ShippingAddress shippingAddress);

    BResponse<Integer> deleteAddress(Integer id);
}
