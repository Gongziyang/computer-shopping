package com.computershop.demo.service.impl;

import com.computershop.demo.mapper.TabletComputerMapper;
import com.computershop.demo.service.TabletComputerService;
import com.computershop.demo.vo.BResponse;
import com.computershop.demo.vo.common.TabletComputer;
import com.computershop.demo.vo.common.TabletComputerDetail;
import com.computershop.demo.vo.req.ReqTabletComputer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:54
 * @file: TabletComputerServiceImpl
 * @description: 星期四
 */
@Service
public class TabletComputerServiceImpl implements TabletComputerService {

    @Autowired
    TabletComputerMapper tabletComputerMapper;

    @Override
    public BResponse<List<TabletComputer>> queryTabletComputerByPage(ReqTabletComputer reqTabletComputer) {
        return BResponse.success(tabletComputerMapper.queryTabletComputerByPage());
    }

    @Override
    public BResponse<TabletComputerDetail> queryTabletComputerDetail(Integer id) {
        return BResponse.success(tabletComputerMapper.queryTabletComputerDetail(id));
    }


}
