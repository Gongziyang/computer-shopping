package com.computershop.demo.mapper;

import com.computershop.demo.vo.common.Order;
import com.computershop.demo.vo.req.ReqOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 17:15
 * @file: OrderMapper
 * @description: 星期二
 */
@Mapper
public interface OrderMapper {

    /**
     * 分页查询订单信息
     * **/
    List<Order> queryOrderPage(ReqOrder request);

    /**
     * 通过id删除订单
     * @param orderId 订单id
     * **/
    int deleteOrderById(int orderId);

    int itemSize(ReqOrder request);

    /** 创建订单 **/
    int createOrderInfo(Order order);

    Order queryOrderById(Integer orderId);

    int payActual(Map orderInfo);
}
