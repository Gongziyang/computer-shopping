package com.computershop.demo.mapper;

import com.computershop.demo.vo.common.TabletComputer;
import com.computershop.demo.vo.common.TabletComputerDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:46
 * @file: TabletComputerMapper
 * @description: 星期四
 */
@Mapper
public interface TabletComputerMapper {

    List<TabletComputer> queryTabletComputerByPage();

    TabletComputerDetail queryTabletComputerDetail(Integer id);

}
