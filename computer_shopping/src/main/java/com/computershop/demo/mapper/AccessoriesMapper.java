package com.computershop.demo.mapper;

import com.computershop.demo.vo.common.Accessories;
import com.computershop.demo.vo.common.AccessoriesDetail;
import com.computershop.demo.vo.req.ReqAccessories;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:47
 * @file: AccessoriesMapper
 * @description: 星期四
 */
@Mapper
public interface AccessoriesMapper {

    List<Accessories> queryAccessoriesByPage(ReqAccessories reqAccessories);

    AccessoriesDetail queryAccessorieDetail(Integer id);
}
