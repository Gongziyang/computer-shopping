package com.computershop.demo.mapper;

import com.computershop.demo.vo.common.Computer;
import com.computershop.demo.vo.common.ComputerDetail;
import com.computershop.demo.vo.common.ShippingAddress;
import com.computershop.demo.vo.req.ReqComputer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 17:15
 * @file: ShippingMapper
 * @description: 星期二
 */
@Mapper
public interface ShippingMapper {

     List<ShippingAddress> queryShippingAddress();

     int itemSize();

     int insertAddress(ShippingAddress shippingAddress);

     int deleteAddress(Integer id);
}
