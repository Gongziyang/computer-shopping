package com.computershop.demo.mapper;

import com.computershop.demo.vo.common.Computer;
import com.computershop.demo.vo.common.ComputerDetail;
import com.computershop.demo.vo.req.ReqComputer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 17:14
 * @file: ComputerMapper
 * @description: 星期二
 */
@Mapper
public interface ComputerMapper {

    List<Computer> queryComputerByPage(ReqComputer reqComputer);

    ComputerDetail queryComputerDetail(Integer computerId);

    int itemSize(ReqComputer computer);

}
