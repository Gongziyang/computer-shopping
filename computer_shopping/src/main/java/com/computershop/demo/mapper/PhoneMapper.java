package com.computershop.demo.mapper;

import com.computershop.demo.vo.common.Computer;
import com.computershop.demo.vo.common.ComputerDetail;
import com.computershop.demo.vo.common.Phone;
import com.computershop.demo.vo.common.PhoneDetail;
import com.computershop.demo.vo.req.ReqComputer;
import com.computershop.demo.vo.req.ReqPhone;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/3 15:48
 * @file: PhoneMapper
 * @description: 星期四
 */
@Mapper
public interface PhoneMapper {

    List<Phone> queryPhoneByPage(ReqPhone reqPhone);

    PhoneDetail queryPhoneDetail(Integer phoneId);

    int itemSize();
}
