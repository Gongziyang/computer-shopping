package com.computershop.demo.aop;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 19:30
 * @file: ExceptionHandler
 * @description: 星期二
 */
@ControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String, Object> handle(Exception e) {
        Map<String, Object> params = new HashMap<>(3);
        params.put("Ret", 1);
        params.put("Msg", e.getMessage());
        params.put("Data", null);
        return params;
    }

}
